package Ex3Lab7;

import java.util.concurrent.CountDownLatch;

public class Fir1 extends Thread {

    Object P6, P10;
    int sleep_min, sleep_max, wait;
	private CountDownLatch countDownLatch;

    public Fir1(Object P6, Object P10, CountDownLatch countDownLatch, int sleep_min, int sleep_max, int wait) {
		this.countDownLatch = countDownLatch;
        this.P6 = P6;
        this.P10 = P10;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.wait = wait;

    }

    public void run() {
        System.out.println(this.getName() + " Locatia : " + "P0");
        Tranzitie T5 = new Tranzitie();
        T5.activity(wait,5);

        System.out.println(this.getName() + " Locatia : " + "P1" + " sleep_min:" + sleep_min + " sleepmax:" + sleep_max);

        synchronized (P10) {
            P10.notify();
        }
        synchronized (P6) {
            P6.notify();
        }

        int k = (int) Math.round(Math.random() * (sleep_max - sleep_min) + sleep_min);
        for (int i = 0; i < k * 100000; i++) {
            i++;
            i--;
        }

        Tranzitie T6 = new Tranzitie();
        T6.activity(6);

        System.out.println(this.getName() + " Locatia : " + "P2");

		countDownLatch.countDown();

		try {
			countDownLatch.await();
		} catch (InterruptedException ignored) {
		}
    }
}
