package Ex3Lab7;

import java.util.concurrent.CountDownLatch;

public class Fir3 extends Thread {

    Object P10;
    int sleep_min, sleep_max,y;
    private CountDownLatch countDownLatch;

    public Fir3(Object P10, CountDownLatch countDownLatch, int sleep_min, int sleep_max, int y) {
        this.countDownLatch = countDownLatch;
        this.P10 = P10;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.y=y;


    }

    public void run() {
        System.out.println(this.getName() + " Locatia : " + "P7");

        Tranzitie T12 = new Tranzitie();
        T12.activity(y,12);
        try {

            synchronized (P10) {
                P10.wait();
                System.out.println(this.getName() + " Locatia : " + "P8" + " sleep_min:" + sleep_min + " sleepmax:" + sleep_max);
            }

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int k = (int) Math.round(Math.random() * (sleep_max - sleep_min) + sleep_min);
        for (int i = 0; i < k * 100000; i++) {
            i++;
            i--;
        }

        Tranzitie T13 = new Tranzitie();
        T13.activity(13);

        System.out.println(this.getName() + " Locatia : " + "P9");

        countDownLatch.countDown();

        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }

    }
}
