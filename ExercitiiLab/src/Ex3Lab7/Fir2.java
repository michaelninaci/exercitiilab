package Ex3Lab7;

import java.util.concurrent.CountDownLatch;

public class Fir2 extends Thread {

    Object P6;
    int sleep_min, sleep_max,x;
    private CountDownLatch countDownLatch;

    public Fir2(Object P6, CountDownLatch countDownLatch, int sleep_min, int sleep_max, int x) {
        this.countDownLatch = countDownLatch;
        this.P6 = P6;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.x=x;


    }

    public void run() {
        System.out.println(this.getName() + " Locatia : " + "P3");

        Tranzitie T7 = new Tranzitie();
        T7.activity(x,7);

        try {

            synchronized (P6) {
                P6.wait();
                System.out.println(this.getName() + " Locatia : " + "P4" + " sleep_min:" + sleep_min + " sleepmax:" + sleep_max);
            }

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        int k = (int) Math.round(Math.random() * (sleep_max - sleep_min) + sleep_min);
        for (int i = 0; i < k * 100000; i++) {
            i++;
            i--;
        }


        Tranzitie T8 = new Tranzitie();
        T8.activity(8);

        System.out.println(this.getName() + " Locatia : " + "P5");

        countDownLatch.countDown();

        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }
    }
}
