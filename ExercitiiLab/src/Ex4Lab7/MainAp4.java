package Ex4Lab7;

import java.util.concurrent.Semaphore;

public class MainAp4 {

	static final Semaphore semaphore = new Semaphore(2);

	public static void main(String[] args) {


		Fir1 fir1 = new Fir1( 3, 6, 5);

		Fir2 fir2 = new Fir2( 4, 7, 3);


		Fir3 fir3 = new Fir3( 5, 7, 6);
		
		fir1.start();
		fir2.start();
		fir3.start();
	}
}
