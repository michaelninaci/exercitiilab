package Ex4Lab7;

import static Ex4Lab7.MainAp4.*;

public class Fir2 extends Thread {


	private int sleep_min, sleep_max, wait;

	Fir2(int sleep_min, int sleep_max, int wait) {

		this.sleep_min = sleep_min;
		this.sleep_max = sleep_max;
		this.wait=wait;
	}

	public void run() {
		for (;;) {
			System.out.println(this.getName() +" Locatia :"+ "P0");
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

				System.out.println(this.getName() +" Locatia :"+ "P1");
				int k = (int) Math.round(Math.random() * (sleep_max - sleep_min) + sleep_min);
				for (int i = 0; i < k * 100000; i++) {
					i++;
					i--;
				}

			semaphore.release();

			System.out.println(this.getName() +" Locatia :"+ "P2");


			try {
				Thread.sleep(wait*1000);
			} catch (InterruptedException ignored) {


			}

			System.out.println(this.getName() +" Locatia :"+ "P3");
		}
	}
}
